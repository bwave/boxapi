#ifndef EVENTDISPTCHER_H_
# define EVENTDISPATCHER_H_

#include "Event.h"

namespace BoxAPI
{
	namespace Module
	{
		namespace Event
		{
			class EventDispatcher
			{
			public:
				~EventDispatcher();

				static EventDispatcher&	Instance();

				void					start();
				void					stop();

				void					addSubscriber(Subscriber &sub);
				void					addSubscriber(Subscriber &sub, std::shared_ptr<IModule> module);
				void					addSubscriber(Subscriber &sub, std::shared_ptr<IModule> module, int eventId);

				void					dispatchEvent(EventData &data);

			private:
				EventDispatcher();
				EventDispatcher& operator=(const EventDispatcher&);
				EventDispatcher(const EventDispatcher&);

				std::thread				_processThread;
				void					process();

				static EventDispatcher												m_instance;
				std::multimap<std::shared_ptr<IModule>, Subscriber>					_moduleSubsribers;
				std::multimap<std::pair<std::shared_ptr<IModule>, int>, Subscriber>	_eventSubsribers;
				std::queue<EventData>												_events;

				bool					_started;
				bool					_destructed;

				std::mutex				_eventmutex;
				std::mutex				_submutex;
				std::condition_variable	_eventcv;
			};
		}
	}
}

#endif	/* !EVENTDISPATCHER_H_ */