#include "stdafx.h"
#include "ZWay.h"

BoxAPI::Techno::ZWave::ZWayLib::ZWayLib()
{

}

BoxAPI::Techno::ZWave::ZWayLib::~ZWayLib()
{
	zway_stop(_zway);
	zway_terminate(&_zway);
	zlog_close(_logger);
}

static void print_zway_terminated(ZWay zway, void* arg)
{
	zway_log(zway, Information, ZSTR("Z-Way terminated"));
}

bool		BoxAPI::Techno::ZWave::ZWayLib::init()
{
	std::cout << "test 1" << std::endl;
	_logger = zlog_create(stdout, Critical);
	std::cout << "test 2" << std::endl;
	ZWError r = zway_init(&_zway, ZSTR("/dev/ttyAMA0"),
		"/opt/z-way-server/config",
		"/opt/z-way-server/translations",
		"/opt/z-way-server/ZDDX", "BWave", _logger);
	std::cout << "test 3" << std::endl;
	_events = new EventManager(_zway);
	if (r != NoError)
	{
		std::cout << r << std::endl;
		zway_log_error(_zway, Critical, "Failed to init ZWay", r);
		return (false);
	}
	std::cout << "test 4" << std::endl;
	std::cout << "test 5" << std::endl;
	r = zway_start(_zway, print_zway_terminated, nullptr);
	std::cout << "test 6" << std::endl;
	if (r != NoError)
	{
		zway_log_error(_zway, Critical, "Failed to start ZWay", r);
		return false;
	}
	std::cout << "test 7" << std::endl;
	r = zway_discover(_zway);
	std::cout << "test 8" << std::endl;
	if (r != NoError)
	{
		zway_log_error(_zway, Critical, "Failed to negotiate with Z-Wave stick", r);
		return false;
	}

	ZWDevicesList			dlist;
	ZWInstancesList			ilist;
	ZWCommandClassesList	clist;

	if ((dlist = zway_devices_list(_zway)) == NULL)
	{
		std::cout << "Unable to get device list" << std::endl;
		return false;
	}
	for (int i = 0; dlist[i] != NULL; i++)
	{
		if ((ilist = zway_instances_list(_zway, dlist[i])) == NULL)
		{
			std::cout << "Unable to get instance list" << std::endl;
			continue ;
		}
		for (int n = 0; n == 0 || ilist[n] != NULL; n++)
		{
			if ((clist = zway_command_classes_list(_zway , dlist[i], ilist[n])) == NULL)
			{
				std::cout << "Unable to get commandclass list" << std::endl;
				continue ;
			}
			for (int j = 0; clist[j] != NULL; j++)
				_events->addCommandEvent(dlist[i], ilist[n], clist[j]);
			zway_command_classes_list_free(clist);
		}
		zway_instances_list_free(ilist);
	}
	zway_devices_list_free(dlist);
	zway_device_add_callback(_zway, DeviceAdded | DeviceRemoved | InstanceAdded | InstanceRemoved | CommandAdded | CommandRemoved, BoxAPI::Techno::ZWave::ZWayLib::print_D_I_CC_event, static_cast<void*>(this));
	std::cout << "test 9" << std::endl;
	return (true);
}

// ZWay Callbacks

void					BoxAPI::Techno::ZWave::ZWayLib::successCallback(const ZWay zway, ZWBYTE functionId, void* arg)
{
	BoxAPI::CallBackCommandResult	*callback = static_cast<BoxAPI::CallBackCommandResult *>(arg);
	BoxAPI::CommandResult			result;

	if (*callback == false)
		return;
	result.Flag = Ok;
	(*callback)(result);
	delete callback;

}
void					BoxAPI::Techno::ZWave::ZWayLib::failureCallback(const ZWay zway, ZWBYTE functionId, void* arg)
{
	BoxAPI::CallBackCommandResult	*callback = static_cast<BoxAPI::CallBackCommandResult *>(arg);
	BoxAPI::CommandResult			result;

	if (*callback == false)
		return;
	result.Flag = ResultFlag::CommandError;
	(*callback)(result);
	delete callback;
}

void					BoxAPI::Techno::ZWave::ZWayLib::print_D_I_CC_event(const ZWay zway, ZWDeviceChangeType type, ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id, void *arg)
{
	BoxAPI::Techno::ZWave::ZWayLib *z = static_cast<BoxAPI::Techno::ZWave::ZWayLib *>(arg);

	switch (type)
	{
	case DeviceRemoved:
		z->_events->removeEvents(node_id);
	case InstanceRemoved:
		z->_events->removeEvents(node_id, instance_id);
		break;
	case CommandAdded:
	{
		std::cout << "add command event (zway) [" << (int)node_id << "," << (int)instance_id << "," << (int)command_id << "]" << std::endl;
		z->_events->addCommandEvent(node_id, instance_id, command_id);
		break;
	}
	case CommandRemoved:
		z->_events->removeEvents(node_id, instance_id, command_id);
		break;
	}

}

// Command Class Basic

void					BoxAPI::Techno::ZWave::ZWayLib::basicSet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;

	if (params.size() != 4 || !params[1].isInt() || !params[2].isInt() || !params[3].isInt())
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
	ZWError e = zway_cc_basic_set(_zway,
		static_cast<unsigned char>(params[1].asInt()), static_cast<unsigned char>(params[2].asInt()), static_cast<unsigned char>(params[3].asInt()),
		&BoxAPI::Techno::ZWave::ZWayLib::successCallback, &BoxAPI::Techno::ZWave::ZWayLib::failureCallback, new BoxAPI::CallBackCommandResult(callback));
	if (e == InvalidArg)
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
}

void					BoxAPI::Techno::ZWave::ZWayLib::basicGet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;

	if (params.size() != 3 || !params[1].isInt() || !params[2].isInt())
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
	ZWError e = zway_cc_basic_get(_zway,
		static_cast<unsigned char>(params[1].asInt()), static_cast<unsigned char>(params[2].asInt()),
		&BoxAPI::Techno::ZWave::ZWayLib::successCallback, &BoxAPI::Techno::ZWave::ZWayLib::failureCallback, new BoxAPI::CallBackCommandResult(callback));
	if (e == InvalidArg)
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
}

// ZWEXPORT ZWError zway_fc_add_node_to_network(ZWay zway, ZWBOOL startStop, ZWBOOL highPower,  ZJobCustomCallback successCallback, ZJobCustomCallback failureCallback, void* callbackArg);
// Function classes

void					BoxAPI::Techno::ZWave::ZWayLib::addNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;

	if (params.size() != 2 || !params[1].isInt())
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
	ZWError e = zway_fc_add_node_to_network(_zway,
		static_cast<unsigned char>(params[1].asInt()), TRUE,
		&BoxAPI::Techno::ZWave::ZWayLib::successCallback, &BoxAPI::Techno::ZWave::ZWayLib::failureCallback, new BoxAPI::CallBackCommandResult(callback));
	if (e == InvalidArg)
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
}

void					BoxAPI::Techno::ZWave::ZWayLib::removeNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;

	if (params.size() != 2 || !params[1].isInt())
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
	ZWError e = zway_fc_remove_node_from_network(_zway,
		static_cast<unsigned char>(params[1].asInt()), TRUE,
		&BoxAPI::Techno::ZWave::ZWayLib::successCallback, &BoxAPI::Techno::ZWave::ZWayLib::failureCallback, new BoxAPI::CallBackCommandResult(callback));
	if (e == InvalidArg)
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		if (callback)
			callback(res);
		return;
	}
}

extern "C" BoxAPI::Techno::ITechno *create_techno()
{
	return new BoxAPI::Techno::ZWave::ZWayLib;
}