#ifndef MODULE_H_
# define MODULE_H_

#include "Command.h"

namespace BoxAPI
{
	namespace Module
	{
		class IModule
		{
		public:
			virtual ~IModule() {}

			virtual int						getId() const = 0;
			virtual const std::string		&getName() const = 0;
			virtual void					execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback = nullptr) = 0;
		};
	}
}

#endif /* !MODULE_H_ */