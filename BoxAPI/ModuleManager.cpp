#include "stdafx.h"
#include "ModuleManager.h"

BoxAPI::Module::ModuleManager::ModuleManager()
{

}

BoxAPI::Module::ModuleManager::~ModuleManager()
{
	for (std::shared_ptr<BoxAPI::Module::IModule> m : this->_modules)
		m.reset();
	_modules.clear();
}

const std::vector<std::shared_ptr<BoxAPI::Module::IModule>>	&BoxAPI::Module::ModuleManager::getModules() const
{
	return _modules;
}

std::shared_ptr<BoxAPI::Module::IModule>					BoxAPI::Module::ModuleManager::getModule(const int id) const
{
	for (std::shared_ptr<BoxAPI::Module::IModule> m : this->_modules)
		if (m->getId() == id)
			return (m);
	return nullptr;
}

std::shared_ptr<BoxAPI::Module::IModule>					BoxAPI::Module::ModuleManager::getModule(const std::string &name) const
{
	for (std::shared_ptr<BoxAPI::Module::IModule> m : this->_modules)
		if (m->getName() == name)
			return (m);
	return nullptr;
}

bool												BoxAPI::Module::ModuleManager::addModule(std::shared_ptr<BoxAPI::Module::IModule> module)
{
	if (module && module != nullptr
		&& this->getModule(module->getId()) == nullptr 
		&& this->getModule(module->getName()) == nullptr)
	{
		_modules.push_back(module);
		return (true);
	}
	return false;
}
