#include "stdafx.h"
#include "Variant.h"

BoxAPI::Variant::Variant(const Variant &other)
	: _type(other._type), _value(other._value)
{
	if (_type == VariantType::String)
	{
		_value.c = new char[std::strlen(other._value.c) + 1];
		std::strcpy(_value.c, other._value.c);
		_value.c[std::strlen(other._value.c)] = 0;
	}
}

BoxAPI::Variant::Variant(const std::string &str)
	: _type(VariantType::String)
{
	_value.c = new char[str.length() + 1];
	std::strcpy(_value.c, str.c_str());
	_value.c[str.length()] = 0;
}

BoxAPI::Variant::Variant(const char *c)
	: _type(VariantType::String)
{
	_value.c = new char[std::strlen(c) + 1];
	std::strcpy(_value.c, c);
	_value.c[std::strlen(c)] = 0;
}

BoxAPI::Variant::Variant(int i)
	: _type(VariantType::Int)
{
	_value.i = i;
}

BoxAPI::Variant::Variant(bool b)
	: _type(VariantType::Bool)
{
	_value.b = b;
}

BoxAPI::Variant::Variant()
	: _type(Empty)
{

}

BoxAPI::Variant::~Variant()
{
	if (_type == VariantType::String)
		delete[] _value.c;
}

BoxAPI::Variant&	BoxAPI::Variant::operator=(const Variant &other)
{
	if (_type == VariantType::String)
		delete[] _value.c;
	_value = other._value;
	_type = other._type;
	if (_type == VariantType::String)
	{
		_value.c = new char[std::strlen(other._value.c) + 1];
		std::strcpy(_value.c, other._value.c);
		_value.c[std::strlen(other._value.c)] = 0;
	}
	return *this;
}

BoxAPI::Variant&	BoxAPI::Variant::operator=(const std::string &str)
{
	if (_type == VariantType::String)
		delete[] _value.c;
	_value.c = new char[str.length() + 1];
	std::strcpy(_value.c, str.c_str());
	_value.c[str.length()] = 0;
	_type = VariantType::String;
	return *this;
}

BoxAPI::Variant&	BoxAPI::Variant::operator=(int i)
{
	if (_type == VariantType::String)
		delete[] _value.c;
	_value.i = i;
	_type = VariantType::Int;
	return *this;
}

BoxAPI::Variant&	BoxAPI::Variant::operator=(const char *c)
{
	if (_type == VariantType::String)
		delete[] _value.c;
	_value.c = new char[std::strlen(c) + 1];
	std::strcpy(_value.c, c);
	_value.c[std::strlen(c)] = 0;
	_type = VariantType::String;
	return *this;
}

BoxAPI::Variant&	BoxAPI::Variant::operator=(bool b)
{
	if (_type == VariantType::String)
		delete[] _value.c;
	_value.b = b;
	_type = VariantType::Bool;
	return *this;
}

bool			BoxAPI::Variant::isInt() const
{
	return (_type == VariantType::Int);
}

bool			BoxAPI::Variant::isString() const
{
	return (_type == VariantType::String);
}

bool			BoxAPI::Variant::isBool() const
{
	return (_type == VariantType::Bool);
}

bool			BoxAPI::Variant::isEmpty() const
{
	return (_type == VariantType::Empty);
}

std::string		BoxAPI::Variant::asString() const
{
	return _value.c;
}

const char		*BoxAPI::Variant::asCString() const
{
	return _value.c;
}

int				BoxAPI::Variant::asInt() const
{
	return _value.i;
}

bool			BoxAPI::Variant::asBool() const
{
	return _value.b;
}