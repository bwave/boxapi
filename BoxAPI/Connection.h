#ifndef CONNECTION_H_
# define CONENCTION_H_

#include "Command.h"

namespace BoxAPI
{
	class BWave;

	class Connection
	{
	public:
		enum Status
		{
			Connecting,
			Open,
			Failed,
			Closed
		};

		Connection();
		~Connection();

		bool			connect(const std::string &uri);
		bool			stop();

	private:
		typedef websocketpp::client<websocketpp::config::asio_client>	WebSocketClient;

		void								returnMessage(Json::Value &message, BoxAPI::CommandResult	&result);

		// WebSocket handlers
		void								onOpen(WebSocketClient *c, websocketpp::connection_hdl hdl);
		void								onFail(WebSocketClient *c, websocketpp::connection_hdl hdl);
		void								onClose(WebSocketClient *c, websocketpp::connection_hdl hdl);
		void								onMessage(websocketpp::connection_hdl, WebSocketClient::message_ptr msg);

		WebSocketClient													m_endpoint;
		websocketpp::connection_hdl										m_hdl;
		websocketpp::lib::shared_ptr<websocketpp::lib::thread>			m_thread;
		Status															m_status;
	};
}

#endif /* !CONNECTION_H_ */