#include "stdafx.h"
#include "EventManager.h"

BoxAPI::Techno::ZWave::EventManager::EventManager(ZWay zway)
	: _zway(zway)
{
	_cmd[0x20] = &EventManager::basicEvents;
	_cmd[0x31] = &EventManager::sensorMultiLevelEvents;
}

BoxAPI::Techno::ZWave::EventManager::~EventManager()
{
}

void BoxAPI::Techno::ZWave::EventManager::removeEvents(ZWBYTE node_id)
{
	_events.erase(
		std::remove_if(_events.begin(), _events.end(),
			[&](std::unique_ptr<EventMonitor> const &p) { return (p->getDevice() == node_id); }));
}

void BoxAPI::Techno::ZWave::EventManager::removeEvents(ZWBYTE node_id, ZWBYTE instance_id)
{
	_events.erase(
		std::remove_if(_events.begin(),_events.end(),
			[&](std::unique_ptr<EventMonitor> const &p) { return (p->getDevice() == node_id && p->getInst() == instance_id); }));
}

void BoxAPI::Techno::ZWave::EventManager::removeEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id)
{
	_events.erase(
		std::remove_if(_events.begin(), _events.end(),
			[&](std::unique_ptr<EventMonitor> const &p) { return (p->getDevice() == node_id && p->getInst() == instance_id && p->getCmd() == command_id); }));
}

void BoxAPI::Techno::ZWave::EventManager::addCommandEvent(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id)
{
	std::map<ZWBYTE, void (EventManager::*)(ZWBYTE, ZWBYTE, ZWBYTE)>::const_iterator it = _cmd.find(command_id);
	if (it == _cmd.end())
		return;
	(this->*it->second)(node_id, instance_id, command_id);
}

void BoxAPI::Techno::ZWave::EventManager::basicEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id)
{
	return;
}

void BoxAPI::Techno::ZWave::EventManager::sensorMultiLevelEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id)
{
	ZDataHolder data;
	std::string path;
	std::unique_ptr<EventMonitor>	e;

	zdata_acquire_lock(ZDataRoot(_zway));
	data = zway_find_device_instance_cc_data(_zway, node_id, instance_id, command_id, NULL);
	ZDataIterator child = zdata_first_child(data);
	while (child != NULL)
	{
		ZDataIterator c = zdata_first_child(child->data);
		if (c != NULL)
		{
			path = zdata_get_name(child->data);
			path += ".val";
			char *cpath = zdata_get_path(data);
			std::cout << "[EVENTS] Find a sensotrMultilevel [" << cpath << "]" << std::endl;
			free(cpath);
			e.reset(new EventMonitor(_zway, node_id, instance_id, command_id, path.c_str()));
			_events.push_back(std::move(e));
		}
		child = zdata_next_child(child);
	}
	zdata_release_lock(ZDataRoot(_zway));
	return;
}