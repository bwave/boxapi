#ifndef AZWAY_H_
# define AZWAY_H_

#include "AZWave.h"
#include <ZWayLib.h>
#include <ZLogging.h>
#include <errno.h>
#include "EventManager.h"

namespace BoxAPI
{
	namespace Techno
	{
		namespace ZWave
		{
			class ZWayLib : public AZWave
			{
			public:
				ZWayLib();
				virtual	~ZWayLib();

				bool						init();

			private:
				// Comand Class Basic
				void						basicSet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr);
				void						basicGet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr);

				// Function Classes
				void						addNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr);
				void						removeNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr);

				// ZWay callbacks
				static void					successCallback(const ZWay zway, ZWBYTE functionId, void* arg);
				static void					failureCallback(const ZWay zway, ZWBYTE functionId, void* arg);

				static void					print_D_I_CC_event(const ZWay zway, ZWDeviceChangeType type, ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id, void *arg);

				ZWLog						_logger;
				ZWay						_zway;
				EventManager				*_events;
			};
		}
	}
}

extern "C" BoxAPI::Techno::ITechno *create_techno();

#endif /* !AZWAY_H_ */