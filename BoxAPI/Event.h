#ifndef EVENT_H_
# define EVENT_H_

#include "IModule.h"
#include "Variant.h"

namespace BoxAPI
{
	namespace Module
	{
		namespace Event
		{
			struct EventData
			{
				std::shared_ptr<IModule>	from;
				int							eventId;
				std::vector<Variant>		Params;
			};

			typedef std::function<void(const BoxAPI::Module::Event::EventData &)>	EventHandler;
			typedef std::function<bool(const BoxAPI::Module::Event::EventData &)>	ConditionHandler;

			class Subscriber {
			public:
				Subscriber(EventHandler ehandler = nullptr, ConditionHandler chandler = nullptr);
				Subscriber& operator=(const Subscriber&);
				Subscriber(const Subscriber&);

				void	operator()(const BoxAPI::Module::Event::EventData &);

				void	setAction(EventHandler handler);
				void	setCondition(ConditionHandler handler);

			private:
				bool	defaultCondition(const BoxAPI::Module::Event::EventData &);
				void	defaultAction(const BoxAPI::Module::Event::EventData &);

				EventHandler		_method;
				ConditionHandler	_condition;
				
			};
		}
	}
}

#endif /* !EVENT_H_ */