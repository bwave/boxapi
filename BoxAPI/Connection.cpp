#include "stdafx.h"
#include "Command.h"
#include "BWave.h"

BoxAPI::Connection::Connection()
	: m_status(Connecting)
{
	m_endpoint.clear_access_channels(websocketpp::log::alevel::all);
	m_endpoint.clear_error_channels(websocketpp::log::elevel::all);
	m_endpoint.init_asio();
	m_endpoint.start_perpetual();
	m_thread.reset(new websocketpp::lib::thread(&WebSocketClient::run, &m_endpoint));
}

BoxAPI::Connection::~Connection()
{
	
}

bool					BoxAPI::Connection::connect(const std::string &uri)
{
	websocketpp::lib::error_code ec;
	WebSocketClient::connection_ptr con;
	con = m_endpoint.get_connection(uri, ec);
	if (ec)
	{
		std::cout << "> Connect initialization error: " << ec.message() << std::endl;
		return false; 
	}
	this->m_hdl = con->get_handle();
	con->set_open_handler(websocketpp::lib::bind(
		&BoxAPI::Connection::onOpen,
		this,
		&m_endpoint,
		websocketpp::lib::placeholders::_1
		));
	con->set_fail_handler(websocketpp::lib::bind(
		&BoxAPI::Connection::onFail,
		this,
		&m_endpoint,
		websocketpp::lib::placeholders::_1
		));
	con->set_close_handler(websocketpp::lib::bind(
		&BoxAPI::Connection::onClose,
		this,
		&m_endpoint,
		websocketpp::lib::placeholders::_1
		));
	con->set_message_handler(websocketpp::lib::bind(
		&BoxAPI::Connection::onMessage,
		this,
		websocketpp::lib::placeholders::_1,
		websocketpp::lib::placeholders::_2
		));
	m_endpoint.connect(con);
	return true;
}

bool					BoxAPI::Connection::stop()
{
	//close(_socket);
	return true;
}

void BoxAPI::Connection::onOpen(WebSocketClient *c, websocketpp::connection_hdl hdl) 
{
	m_status = Open;

	/*WebSocketClient::connection_ptr con = c->get_con_from_hdl(hdl);
	m_server = con->get_response_header("Server");*/
}

void BoxAPI::Connection::onFail(WebSocketClient *c, websocketpp::connection_hdl hdl)
{
	m_status = Failed;

	/*WebSocketClient::connection_ptr con = c->get_con_from_hdl(hdl);
	m_server = con->get_response_header("Server");
	m_error_reason = con->get_ec().message();*/
}

void BoxAPI::Connection::onClose(WebSocketClient *c, websocketpp::connection_hdl hdl)
{
	m_status = Closed;
	/*WebSocketClient::connection_ptr con = c->get_con_from_hdl(hdl);
	std::stringstream s;
	s << "close code: " << con->get_remote_close_code() << " ("
		<< websocketpp::close::status::get_string(con->get_remote_close_code())
		<< "), close reason: " << con->get_remote_close_reason();
	m_error_reason = s.str();*/
}

void	BoxAPI::Connection::returnMessage(Json::Value &message, BoxAPI::CommandResult &result)
{
	Json::Value			results(Json::arrayValue);
	Json::FastWriter	writer;
	int					i = 0;

	message["flag"] = result.Flag;
	message["results"] = results;
	for (BoxAPI::Variant &var : result.Results)
	{
		if (var.isInt())
			results[i++] = var.asInt();
		else if (var.isString())
			results[i++] = var.asString();
	}
	websocketpp::lib::error_code ec;
	std::cout << "Sending: " << writer.write(message) << std::endl;
	m_endpoint.send(m_hdl, writer.write(message), websocketpp::frame::opcode::text, ec);
	if (ec)
		std::cout << "Fail to send !" << std::endl;
	result.Results.clear();
}

void	BoxAPI::Connection::onMessage(websocketpp::connection_hdl hdl, WebSocketClient::message_ptr msg)
{
	Json::Value				message;
	Json::Reader			reader;
	BoxAPI::CommandResult	result;

	result.Flag = ResultFlag::IncorrectJsonObject;
	std::cout << "Received :" << msg->get_payload() << std::endl;
	if (msg->get_opcode() == websocketpp::frame::opcode::text)
	{
		try
		{
			bool parsingSuccessful = reader.parse(msg->get_payload().c_str(), message);
			if (!parsingSuccessful)
			{
				returnMessage(message, result);
				return;
			}
		}
		catch (...)
		{
			returnMessage(message, result);
			return;
		}
		BWave::Instance().execReq(message, bind(&BoxAPI::Connection::returnMessage, this, message, std::placeholders::_1));
	}
	else
		returnMessage(message, result);
}