#ifndef BWAVE_H_
# define BWAVE_H_

#include "Connection.h"
#include "Command.h"
#include "Event.h"
#include "ModuleManager.h"


namespace BoxAPI
{	
	class BWave
	{
	public:
		~BWave();
		static BWave&						Instance();

		bool								start(std::string &host);
		void								stop();

		Module::ModuleManager				&getModuleManager();
		void								execReq(Json::Value &Packet, CallBackCommandResult callback = nullptr);
	private:

		BWave();
		BWave& operator=(const BWave&);
		BWave(const BWave&);

		Module::ModuleManager				_moduleManager;
		BoxAPI::Connection					_connection;

		static BWave						m_instance;
	};
}

#endif /* !BWAVE_H_ */