#include "stdafx.h"
#include "Controller.h"
#include "ZWay.h"

BoxAPI::Module::Controller::Controller()
	: _id(1), _name("Controller")
{
	// init each techno in the techno folder
	std::cout << "create_techo()" << std::endl;
	BoxAPI::Techno::ITechno *zway = create_techno();
	std::cout << "init()" << std::endl;
	if (!zway->init())
		std::cout << "failed" << std::endl;
	std::cout << "push_back()" << std::endl;
	_technos.push_back(zway);
}

BoxAPI::Module::Controller::~Controller()
{
	for (BoxAPI::Techno::ITechno *techno : _technos)
		delete techno;
}

int						BoxAPI::Module::Controller::getId() const
{
	return (_id);
}

const std::string				&BoxAPI::Module::Controller::getName() const
{
	return (_name);
}

void							BoxAPI::Module::Controller::execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;
	if (!cmd.Params[0].isInt())
	{
		if (callback)
		{
			res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
			callback(res);
		}
		return;
	}
	for (BoxAPI::Techno::ITechno *tech : _technos)
		if (tech->getId() == static_cast<unsigned char >(cmd.Params[0].asInt()))
		{
			tech->execCmd(cmd, callback);
			return;
		}
	if (callback)
	{
		res.Flag = BoxAPI::ResultFlag::IncorrectCommandParam;
		callback(res);
	}
	return;
}

extern "C" BoxAPI::Module::IModule *create_module()
{
	return new BoxAPI::Module::Controller;
}