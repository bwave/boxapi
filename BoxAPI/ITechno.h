#ifndef ITECHNO_H_
# define ITECHNO_H_

#include "Command.h"

namespace BoxAPI
{
	namespace Techno
	{
		class ITechno
		{
		public:
			virtual					~ITechno() {}

			virtual const std::string			&getName() const = 0;
			virtual int							getId() const = 0;

			virtual bool						init() = 0;
			virtual void						execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback = nullptr) = 0;
		};
	}
}

#endif /* !ITECHNO_H_ */