#ifndef EVENT_MONITOR_H_
# define EVENT_MONITOR_H_

#include <ZWayLib.h>
#include <ZData.h>

namespace BoxAPI
{
	namespace Techno
	{
		namespace ZWave
		{
			class EventMonitor
			{
			public:
				EventMonitor(ZWay zway, ZWBYTE deviceId, ZWBYTE instId, ZWBYTE cmd, const char *path);
				~EventMonitor();

				ZWBYTE			getDevice() const;
				ZWBYTE			getInst() const;
				ZWBYTE			getCmd() const;

			private:
				static void		CTypeCB(const ZDataRootObject root, ZWDataChangeType type, ZDataHolder data, void *arg);

				void			CBBool(ZDataHolder d);
				void			CBInt(ZDataHolder d);
				void			CBFloat(ZDataHolder d);
				void			CBString(ZDataHolder d);

				ZWay			_zway;
				ZWBYTE			_device;
				ZWBYTE			_inst;
				ZWBYTE			_cmd;
				std::string		_path;

				static std::map<ZWDataType, void (EventMonitor::*)(ZDataHolder)>	_CBType;
			};
		}
	}
}

#endif /* !EVENT_MONITOR_H_ */