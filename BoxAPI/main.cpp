#include "stdafx.h"
#include "BWave.h"
#include "Controller.h"

int main(int ac , char **av)
{
	BoxAPI::Module::IModule	*m = create_module();
	std::shared_ptr<BoxAPI::Module::IModule>	ptr(m);
	if (!BoxAPI::BWave::Instance().getModuleManager().addModule(ptr))
		std::cout << "Fail to add" << std::endl;
	if (ac != 2)
	{
		std::cout << "USAGE: " << av[0] << "ws://HOST:IP" << std::endl;
		return 0;
	}
	std::string host = av[1];
	if (BoxAPI::BWave::Instance().start(host) == false)
	{
		std::cout << "Fail to start" << std::endl;
		return (0);
	}
	std::cout << "Type enter to leave..." << std::endl;
	char c;
	std::cin >> c;
}