#include "stdafx.h"
#include "AZWave.h"

/*
**	Command Class
*/

BoxAPI::Techno::ZWave::AZWave::Commands::Commands()
{
	// Basic Command Class
	_commands[1] = &AZWave::basicGet;
	_commands[2] = &AZWave::basicSet;

	// Function class
	_commands[10] = &AZWave::addNodeToNetwork;
	_commands[11] = &AZWave::removeNodeToNetwork;
}

BoxAPI::Techno::ZWave::AZWave::Commands::~Commands()
{

}

void					BoxAPI::Techno::ZWave::AZWave::Commands::CallCommand(AZWave *zwave, const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback)
{
	CommandResult		res;
	std::map<int, void(AZWave::*)(const std::vector<Variant>, BoxAPI::CallBackCommandResult)>::const_iterator it = _commands.find(cmd.Id);
	if (it == _commands.end())
	{
		res.Flag = ResultFlag::CommandNotFound;
		if (callback)
			callback(res);
		return;
	}
	(zwave->*it->second)(cmd.Params, callback);
}

/*
** AZWave class
*/

BoxAPI::Techno::ZWave::AZWave::Commands BoxAPI::Techno::ZWave::AZWave::_commands = BoxAPI::Techno::ZWave::AZWave::Commands();

BoxAPI::Techno::ZWave::AZWave::~AZWave()
{

}

const std::string			&BoxAPI::Techno::ZWave::AZWave::getName() const
{
	return ("ZWave");
}

int							BoxAPI::Techno::ZWave::AZWave::getId() const
{
	return (1);
}

void					BoxAPI::Techno::ZWave::AZWave::execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback)
{
	BoxAPI::Techno::ZWave::AZWave::_commands.CallCommand(this, cmd, callback);
}