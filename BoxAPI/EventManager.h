#ifndef EVENTMANAGER_H_
# define EVENTMANAGER_H_

#include "stdafx.h"
#include <ZWayLib.h>
#include <ZLogging.h>
#include <ZData.h>
#include "EventMonitor.h"

namespace BoxAPI
{
	namespace Techno
	{
		namespace ZWave
		{
			class EventManager
			{
			public:
				EventManager(ZWay zway);
				~EventManager();


				void		removeEvents(ZWBYTE node_id);
				void		removeEvents(ZWBYTE node_id, ZWBYTE instance_id);
				void		removeEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id);

				void		addCommandEvent(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id);

			private:

				void			basicEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id);
				void			sensorMultiLevelEvents(ZWBYTE node_id, ZWBYTE instance_id, ZWBYTE command_id);

				std::map<ZWBYTE, void (EventManager::*)(ZWBYTE, ZWBYTE, ZWBYTE)>	_cmd;
				std::vector<std::unique_ptr<EventMonitor>>							_events;

				ZWay																_zway;
			};
		}
	}
}

#endif /* !EVENTMANAGER_H_ */