#ifndef VARIANT_H_
# define VARIANT_H_

namespace BoxAPI
{
	class Variant
	{
	public:
		Variant(const Variant &other);
		Variant(const std::string &str);
		Variant(const char *c);
		Variant(int i);
		Variant(bool b);
		Variant();
		~Variant();

		Variant&	operator=(const Variant &other);
		Variant&	operator=(const std::string &str);
		Variant&	operator=(int i);
		Variant&	operator=(const char *c);
		Variant&	operator=(bool b);

		bool			isInt() const;
		bool			isString() const;
		bool			isBool() const;
		bool			isEmpty() const;

		std::string		asString() const;
		const char		*asCString() const;
		int				asInt() const;
		bool			asBool() const;

	private:

		enum VariantType
		{
			Empty,
			Int,
			String,
			Bool
		};

		union
		{
			char		*c;
			int			i;
			bool		b;
		}				_value;
		VariantType		_type;
	};
}

#endif /* !VARIANT_H_ */