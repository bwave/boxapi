#ifndef CONTROLLER_H_
# define CONTROLLER_H_

#include "IModule.h"
#include "Command.h"
#include "ITechno.h"

namespace BoxAPI
{
	namespace Module
	{
		class Controller : public IModule
		{
		public:
			Controller();
			virtual ~Controller();

			int						getId() const;
			const std::string		&getName() const;

			void					execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback = nullptr);

		private:
			const int	 					_id;
			const std::string				_name;

			std::vector<BoxAPI::Techno::ITechno *>	_technos;
		};
	}
}

extern "C" BoxAPI::Module::IModule *create_module();

#endif /* !CONTROLLER_H_ */