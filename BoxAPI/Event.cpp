#include "stdafx.h"
#include "Event.h"

BoxAPI::Module::Event::Subscriber::Subscriber(EventHandler ehandler, ConditionHandler chandler)
	: _method(ehandler), _condition(chandler)
{
}

BoxAPI::Module::Event::Subscriber &BoxAPI::Module::Event::Subscriber::operator=(const Subscriber &s)
{
	this->_method = s._method;
	this->_condition = s._condition;
}

BoxAPI::Module::Event::Subscriber::Subscriber(const Subscriber &s)
	: _method(s._method), _condition(s._condition)
{
}

void BoxAPI::Module::Event::Subscriber::operator()(const BoxAPI::Module::Event::EventData &data)
{
	if (!_condition)
	{
		if (!defaultCondition(data))
			return;
	}
	else if (!_condition(data))
		return;
	if (!_method)
		defaultAction(data);
	else
		_method(data);
}

void BoxAPI::Module::Event::Subscriber::setAction(EventHandler handler)
{
	_method = handler;
}

void BoxAPI::Module::Event::Subscriber::setCondition(ConditionHandler handler)
{
	_condition = handler;
}

bool BoxAPI::Module::Event::Subscriber::defaultCondition(const BoxAPI::Module::Event::EventData &)
{
	return true;
}

void BoxAPI::Module::Event::Subscriber::defaultAction(const BoxAPI::Module::Event::EventData &)
{

}
