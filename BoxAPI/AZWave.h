#ifndef AZWAVE_H_
# define AZWAVE_H_

#include "Command.h"
#include "ITechno.h"

namespace BoxAPI
{
	namespace Techno
	{
		namespace ZWave
		{
			class AZWave : public ITechno
			{
			private:

				class Commands
				{
				public:
					Commands();
					~Commands();

					void				CallCommand(AZWave *zwave, const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback = nullptr);
				private:
					std::map<int, void(AZWave::*)(const std::vector<Variant>, BoxAPI::CallBackCommandResult)> _commands;

				};

			public:
				virtual								~AZWave();

				const std::string					&getName() const;
				int									getId() const;

				virtual bool						init() = 0;
				void								execCmd(const BoxAPI::Command &cmd, BoxAPI::CallBackCommandResult callback = nullptr);

			protected:
				// Comand Class Basic
				virtual void						basicSet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr) = 0;
				virtual void						basicGet(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr) = 0;

				// Function Classes
				virtual void						addNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr) = 0;
				virtual void						removeNodeToNetwork(const std::vector<Variant> params, BoxAPI::CallBackCommandResult callback = nullptr) = 0;

			private:
				static Commands						_commands;
				//const std::string					_name = "ZWave";
			};
		}
	}
}

#endif /* !AZWAVE_H_ */