#define ASIO_STANDALONE

#include "websocketpp/config/asio_no_tls.hpp"
#include "websocketpp/server.hpp"

#include <functional>
#include <iostream>
#include <thread>

typedef websocketpp::server<websocketpp::config::asio> server;
static websocketpp::connection_hdl	g_hdl;

class utility_server {
public:
	utility_server() {
		// Set logging settings
		m_endpoint.set_error_channels(websocketpp::log::elevel::all);
		m_endpoint.set_access_channels(websocketpp::log::alevel::all ^ websocketpp::log::alevel::frame_payload);

		// Initialize Asio
		m_endpoint.init_asio();

		// Set the default message handler to the echo handler
		m_endpoint.set_message_handler(std::bind(
			&utility_server::echo_handler, this,
			std::placeholders::_1, std::placeholders::_2
			));
		m_endpoint.set_open_handler(std::bind(
			&utility_server::open_handler, this,
			std::placeholders::_1));
	}

	void echo_handler(websocketpp::connection_hdl hdl, server::message_ptr msg) {
		std::cout << "\033[32m" << "Message" << "\033[34m" << "\"" << msg->get_payload() << "\"" << "\033[32m" << " received !" << "\033[0m" << std::endl;
	}

	bool open_handler(websocketpp::connection_hdl hdl) {
		std::cout << "\033[32m" << "New connection !" << "\033[0m" << std::endl;
		g_hdl = hdl;
		return true;
	}

	void run() {
		// Listen on port 9002
		m_endpoint.listen(9002);

		// Queues a connection accept operation
		m_endpoint.start_accept();

		// Start the Asio io_service run loop
		m_endpoint.run();
	}
public:
	server m_endpoint;
};

int main() {
	
	
	utility_server s;
	std::thread	t1(std::bind(&utility_server::run, &s));
	std::string str;
	while (true)
	{
		std::cin >> str;
		websocketpp::lib::error_code ec;
		s.m_endpoint.send(g_hdl, str, websocketpp::frame::opcode::text, ec); // send text message.
		if (ec)
		{
			std::cout << "\033[31m" << "fail to send !" << "\033[0m" << std::endl;
			return 0;
		}
		std::cout << "\033[32m" << "Command" << "\033[34m" << "\"" << str << "\"" << "\033[32m" << " as been sent !" << "\033[0m" << std::endl;
	}
	return 0;
}