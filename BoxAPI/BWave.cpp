#include "stdafx.h"
#include "BWave.h"

BoxAPI::BWave BoxAPI::BWave::m_instance = BoxAPI::BWave();

BoxAPI::BWave::BWave()
{

}

BoxAPI::BWave	&BoxAPI::BWave::operator=(const BWave&)
{

}

BoxAPI::BWave::BWave(const BWave&)
{

}

BoxAPI::BWave::~BWave()
{

}

void		BoxAPI::BWave::stop()
{

}

BoxAPI::BWave& BoxAPI::BWave::Instance()
{
	return m_instance;
}

bool				BoxAPI::BWave::start(std::string &host)
{
	_connection.connect(host);
	return true;
}

BoxAPI::Module::ModuleManager		&BoxAPI::BWave::getModuleManager()
{
	return (_moduleManager);
}

/*
** Private Member Function
 */

void						BoxAPI::BWave::execReq(Json::Value &Packet, CallBackCommandResult callback)
{
	BoxAPI::Command								cmdModule;
	std::shared_ptr<BoxAPI::Module::IModule>	moduleF;
	BoxAPI::CommandResult						result;
	const Json::Value							req = Packet["req"];

	if (!req || !req.isObject())
	{
		result.Flag = ResultFlag::IncorrectJsonObject;
		if (callback)
			callback(result);
		return;
	}

	const Json::Value				module = Packet["req"]["module"];
	const Json::Value				cmd = Packet["req"]["cmd"];
	const Json::Value				params = Packet["req"]["params"];

	if (!module || !cmd || !params || !module.isInt() || !cmd.isInt() || !params.isArray())
	{
		result.Flag = ResultFlag::IncorrectJsonObject;
		if (callback)
			callback(result);
		return;
	}
	if ((moduleF = _moduleManager.getModule(module.asInt())) == nullptr)
	{
		result.Flag = ResultFlag::ModuleNotFound;
		if (callback)
			callback(result);
		return;
	}
	cmdModule.Id = cmd.asInt();
	for (unsigned int index = 0; index < params.size(); ++index)
	{
		if (params[index].isInt())
			cmdModule.Params.push_back(params[index].asInt());
		else if (params[index].isString())
			cmdModule.Params.push_back(params[index].asString());
	}
	moduleF->execCmd(cmdModule, callback);
	cmdModule.Params.clear();
}
