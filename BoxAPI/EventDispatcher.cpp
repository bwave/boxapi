#include "stdafx.h"
#include "EventDispatcher.h"

BoxAPI::Module::Event::EventDispatcher BoxAPI::Module::Event::EventDispatcher::m_instance = BoxAPI::Module::Event::EventDispatcher();

BoxAPI::Module::Event::EventDispatcher::~EventDispatcher()
{
	_started = false;
	_destructed = true;
	_eventcv.notify_one();
	_processThread.join();

	_moduleSubsribers.clear();
	_eventSubsribers.clear();
	//std::swap(_events, std::queue<EventData>());
}

BoxAPI::Module::Event::EventDispatcher::EventDispatcher()
	: _started(false), _destructed(false), _processThread(std::bind(&BoxAPI::Module::Event::EventDispatcher::process, this))
{

}

BoxAPI::Module::Event::EventDispatcher::EventDispatcher(const EventDispatcher &)
{
}

BoxAPI::Module::Event::EventDispatcher &BoxAPI::Module::Event::EventDispatcher::operator=(const EventDispatcher &)
{
	
}

BoxAPI::Module::Event::EventDispatcher & BoxAPI::Module::Event::EventDispatcher::Instance()
{
	return (BoxAPI::Module::Event::EventDispatcher::m_instance);
}

void BoxAPI::Module::Event::EventDispatcher::start()
{
	_started = true;
	_eventcv.notify_one();
}

void BoxAPI::Module::Event::EventDispatcher::stop()
{
	_started = false;
}

void BoxAPI::Module::Event::EventDispatcher::addSubscriber(Subscriber & sub)
{
	std::unique_lock<std::mutex>	lock(_submutex);

	this->_moduleSubsribers.insert(std::make_pair(nullptr, sub));
}

void BoxAPI::Module::Event::EventDispatcher::addSubscriber(Subscriber & sub, std::shared_ptr<IModule> module)
{
	std::unique_lock<std::mutex>	lock(_submutex);
	this->_moduleSubsribers.insert(std::make_pair(module, sub));
}

void BoxAPI::Module::Event::EventDispatcher::addSubscriber(Subscriber & sub, std::shared_ptr<IModule> module, int eventId)
{
	std::unique_lock<std::mutex>	lock(_submutex);
	this->_eventSubsribers.insert(std::make_pair(std::make_pair(module, eventId), sub));
}

void BoxAPI::Module::Event::EventDispatcher::dispatchEvent(EventData &data)
{
	std::unique_lock<std::mutex>	lock(_eventmutex);
	_events.push(data);
	_eventcv.notify_one();
}

void BoxAPI::Module::Event::EventDispatcher::process()
{
	EventData					data;
	while (true)
	{
		{
			std::unique_lock<std::mutex> lock(_eventmutex);
			while (!_destructed && _events.empty())
				_eventcv.wait(lock, [this]() -> bool { return (_started || _destructed); });
			if (_destructed)
				return;
			data = _events.front();
			_events.pop();
		}
		{
			std::unique_lock<std::mutex> lock(_submutex);
			for (std::multimap<std::shared_ptr<IModule>, Subscriber>::iterator it = _moduleSubsribers.begin(); it != _moduleSubsribers.end(); ++it)
				if (it->first == nullptr || it->first == data.from)
					it->second(data);
			for (std::multimap<std::pair<std::shared_ptr<IModule>, int>, Subscriber>::iterator it = _eventSubsribers.begin(); it != _eventSubsribers.end(); ++it)
				if (it->first.first == data.from && it->first.second == data.eventId)
					it->second(data);
			data.Params.clear();
		}

	}
}
