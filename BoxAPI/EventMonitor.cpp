#include "stdafx.h"
#include "EventMonitor.h"
#include <exception>

std::map<ZWDataType, void (BoxAPI::Techno::ZWave::EventMonitor::*)(ZDataHolder)>	BoxAPI::Techno::ZWave::EventMonitor::_CBType;

BoxAPI::Techno::ZWave::EventMonitor::EventMonitor(ZWay zway, ZWBYTE deviceId, ZWBYTE instId, ZWBYTE cmd, const char *path)
	: _zway(zway), _device(deviceId), _inst(instId), _cmd(cmd), _path(path)
{
	ZDataHolder data;
	//zdata_acquire_lock(ZDataRoot(_zway));
	data = zway_find_device_instance_cc_data(_zway, _device, _inst, _cmd, _path.c_str());
	if (ZWError r = zdata_add_callback(data, &BoxAPI::Techno::ZWave::EventMonitor::CTypeCB, FALSE, static_cast<void *>(this)))
		throw std::runtime_error("Fail to add data callback");
	//zdata_release_lock(ZDataRoot(_zway));
	if (EventMonitor::_CBType.empty())
	{
		EventMonitor::_CBType[Boolean] = &BoxAPI::Techno::ZWave::EventMonitor::CBBool;
		EventMonitor::_CBType[Integer] = &BoxAPI::Techno::ZWave::EventMonitor::CBInt;
		EventMonitor::_CBType[Float] = &BoxAPI::Techno::ZWave::EventMonitor::CBFloat;
		EventMonitor::_CBType[String] = &BoxAPI::Techno::ZWave::EventMonitor::CBString;
	}
}

BoxAPI::Techno::ZWave::EventMonitor::~EventMonitor()
{
	ZDataHolder data;
	zdata_acquire_lock(ZDataRoot(_zway));
	data = zway_find_device_instance_cc_data(_zway, _device, _inst, _cmd, _path.c_str());
	if (ZWError r = zdata_remove_callback(data, &BoxAPI::Techno::ZWave::EventMonitor::CTypeCB))
		throw std::runtime_error("Fail to remove data callback");
	zdata_release_lock(ZDataRoot(_zway));
}

ZWBYTE	BoxAPI::Techno::ZWave::EventMonitor::getDevice() const
{
	return _device;
}

ZWBYTE	BoxAPI::Techno::ZWave::EventMonitor::getInst() const
{
	return _inst;
}

ZWBYTE	BoxAPI::Techno::ZWave::EventMonitor::getCmd() const
{
	return _cmd;
}

void		BoxAPI::Techno::ZWave::EventMonitor::CTypeCB(const ZDataRootObject root, ZWDataChangeType type, ZDataHolder data, void *arg)
{
	EventMonitor	*e = static_cast<EventMonitor *>(arg);
	ZWDataType		Ztype;

	if (type != Updated)
		return;
	zdata_get_type(data, &Ztype);
	std::map<ZWDataType, void (EventMonitor::*)(ZDataHolder)>::const_iterator it = EventMonitor::_CBType.find(Ztype);
	if (it == EventMonitor::_CBType.end())
		return;
	(e->*it->second)(data);
}

void			BoxAPI::Techno::ZWave::EventMonitor::CBBool(ZDataHolder d)
{
	bool level;
	ZWBOOL value;

	zdata_get_boolean(d, &value);
	switch (value) {
	case 0:
		level = false;
		break;
	case 1:
		level = true;
		break;
	default:
		return;
	}
	char *path = zdata_get_path(d);
	std::cout << "[EVENT] " << path << " changed to: " << level << std::endl;
	free(path);
}

void			BoxAPI::Techno::ZWave::EventMonitor::CBInt(ZDataHolder d)
{
	int level;

	zdata_get_integer(d, &level);
	char *path = zdata_get_path(d);
	std::cout << "[EVENT] " << path << " changed to: " << level << std::endl;
	free(path);
}

void			BoxAPI::Techno::ZWave::EventMonitor::CBFloat(ZDataHolder d)
{
	float level;

	zdata_get_float(d, &level);
	char *path = zdata_get_path(d);
	std::cout << "[EVENT] " << path << " changed to: " << level << std::endl;
	free(path);
}

void			BoxAPI::Techno::ZWave::EventMonitor::CBString(ZDataHolder d)
{
	std::string level;
	ZWCSTR value;

	zdata_get_string(d, &value);
	level = value;
	char *path = zdata_get_path(d);
	std::cout << "[EVENT] " << path << " changed to: " << level << std::endl;
	free(path);
}