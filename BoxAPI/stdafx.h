#ifndef STDAFX_H_
# define STDAFX_H_

// System include
#include <map>
#include <functional>
#include <vector>
#include <string>
#include <queue>
#include <iostream>
#include <exception>

// external include
#define ASIO_STANDALONE
#include "websocketpp/config/asio_no_tls_client.hpp"
#include "websocketpp/client.hpp"
#include "json/json.h"

#endif /* !STDAFX_H_ */