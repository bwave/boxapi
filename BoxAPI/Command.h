#ifndef COMMAND_H_
# define COMMAND_H_

#include "Variant.h"

namespace BoxAPI
{
	struct Command
	{
		int								Id;
		std::vector<Variant>			Params;
	};

	enum ResultFlag
	{
		Ok = 0,
		IncorrectJsonObject,
		ModuleNotFound,
		CommandNotFound,
		IncorrectCommandParam,
		CommandError
	};

	struct CommandResult
	{
		ResultFlag					Flag;
		std::vector<Variant>		Results;
	};

	typedef std::function<void(BoxAPI::CommandResult &)> CallBackCommandResult;
}

#endif /* !COMMAND_H_ */