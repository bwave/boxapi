#ifndef MODULEMANAGER_H_
# define MODULEMANAGER_H_

#include "IModule.h"
#include "Event.h"

namespace BoxAPI
{
	namespace Module
	{
		class ModuleManager
		{
		public:
			ModuleManager();
			~ModuleManager();

			const std::vector<std::shared_ptr<Module::IModule>>			&getModules() const;
			std::shared_ptr<Module::IModule>							getModule(const int id) const;
			std::shared_ptr<Module::IModule>							getModule(const std::string &name) const;

			bool														addModule(std::shared_ptr<Module::IModule> module);
		private:
			std::vector<std::shared_ptr<Module::IModule>>				_modules;
		};
	}
}

#endif /* !MODULEMANAGER_H_ */